import { App, Stack, StackProps, Duration } from '@aws-cdk/core';
import codedeploy = require('@aws-cdk/aws-codedeploy');
import lambda = require('@aws-cdk/aws-lambda');
import apigateway = require('@aws-cdk/aws-apigateway');
import cloudwatch = require('@aws-cdk/aws-cloudwatch');
import iam = require('@aws-cdk/aws-iam');
import { Statistic } from '@aws-cdk/aws-cloudwatch';

export class LambdaStack extends Stack {
  // This property represents the code that is supplied later by the pipeline. 
  // Because the pipeline needs access to the object, we expose it as a public, read-only property on our class.
  public readonly lambdaCode: lambda.CfnParametersCode;
  constructor(app: App, id: string, props?: StackProps) {
    super(app, id, props);
    let lambdaVersion = 6, aliasName = 'prod';
    // These parameters come from the PipelineDeploymentStack
    this.lambdaCode = lambda.Code.cfnParameters();
    const myApplication = new codedeploy.LambdaApplication(this, 'LambdaApplication', {
      applicationName: 'lambda_application_in_pipeline'
    });
    // Proxy lambda Function (To simulate lambda execution to make some test or convert data before execution)
    const proxy = new lambda.Function(this, 'Proxy', {
      code: this.lambdaCode,
      handler: 'proxy.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      functionName: 'proxy_in_pipeline',
    });
    // Main lambda to simulate an application function
    const handler = new lambda.Function(this, 'Lambda', {
      code: this.lambdaCode,
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      functionName: 'lambda_in_pipeline',
    });
    // Grant invocation permissions in the proxy handler
    handler.grantInvoke(proxy);
    // Version and Alias to manage traffic shiffting
    const version = handler.addVersion(lambdaVersion.toString());
    const alias = new lambda.Alias(this, 'LambdaAlias', {
      aliasName: aliasName,
      version: version,
    });
    // Lambda function to execute before traffic shiffting in the lambda deployment group on CodeDeploy
    const preHook = new lambda.Function(this, 'PreHook', {
      code: this.lambdaCode,
      handler: 'prehook.handler',
      runtime: lambda.Runtime.NODEJS_8_10,
      functionName: 'prehook_in_pipeline',
      environment: {
        CurrentVersion: version.toString()
      }
    });
    // LamdaDeploymentGroup to manage the deployment type (Blue/Green), put alarms and run pre and post hooks functions
    new codedeploy.LambdaDeploymentGroup(this, 'LambdaDeploymentGroup', {
      alias,
      application: myApplication,
      deploymentConfig: codedeploy.LambdaDeploymentConfig.CANARY_10PERCENT_5MINUTES,
      deploymentGroupName: 'Lambda_deployment_group',
      preHook: preHook,
      alarms: [
        // pass some alarms when constructing the deployment group
        // Every 30 minutes the alarm evaluates if the errors count is greater than 6, for two times
        new cloudwatch.Alarm(this, 'ErrorsAlarm', {
          comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
          threshold: 0,
          evaluationPeriods: 2,
          metric: alias.metricErrors(),
          period: Duration.minutes(30),
          statistic: Statistic.SUM,
          alarmName: 'errors_alarm_in_pipeline'
        })
      ]
    });
    // ApiGateway to test lambda function traffic shifting
    const api = new apigateway.RestApi(this, 'RestApi', {
      restApiName: 'lambda_rest_api',
    });
    // You can put 'proxy' instead 'alias' to handle the request before processing in the main lambda
    const getLambdaIntegration = new apigateway.LambdaIntegration(alias);
    api.root.addMethod("GET", getLambdaIntegration);
    // Grant invoke permissions to API gateway service, to allow invoke the function through the alias
    handler.addPermission('InvokePermission', {
      principal: new iam.ServicePrincipal('apigateway.amazonaws.com'),
    })
  }
}
