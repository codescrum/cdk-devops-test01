# pipeline project

This project contains an AWS CDK sample implementation to show how to do lambda function traffic shifting with aliases

This project has tne next folders an files:

- bin - Code for CKD application, where the Stacks are instantiated
- lambda - Code for the main application's Lambda, the proxy and the pre hook functions.
  - index - Main Application's Lambda function
  - proxy - Function to handler data or testing before main lambda execution
  - prehook - Function to make some integration testing before lambda deployment
- lib - Code for stacks in CloudFormation template
  - lambda-stack - Stack to manage the lambda function deployment with an alias, a pre-hook and proxy lambda functions, a Rest Api from API Gateway and an alarm from CloudWatch
  - pipeline-stack - Stack to manage the Pipeline process to deploy or update application services when the code changes

## Prerequisites
To run this sample, you need the following tools.

* AWS CLI - [Install the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configure it with your AWS credentials].
* AWS Account and User - [Create new Account and User](https://cdkworkshop.com/15-prerequisites/200-account.html)
* Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
* AWS CDK Toolkit
  ``` bash
  -> npm install -g aws-cdk
  -> cdk --version
  ```

## Building

1. Create an S3 bucket named 'aws-bucket-for-pipeline-lambda-sample' to save the CodePipeline artifacts (You can create this bucket in the AWS console or in AWS CLI).
2. Create a CodeCommit repository to versioning the CDK source code with the name 'hello-world-lambda-function'
3. Push this content in the new CodeCommit repository(Create git credentials for this in IAM):
    ```bash
      -> git remote add origin URL_REPO
      -> git push -u origin master
    ```
4. Build the javascript files from typescripts
    ```bash   
      -> npm run build
    ```
## Deployment

Useful commands
 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
```bash   
$ cdk deploy PipelineDeployingLambdaStack --require-approval never
```   